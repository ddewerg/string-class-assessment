// String Class Assessment.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "StrLib.h"
#include <iostream>
#include <fstream>

void unitTest()
{
	String s, s2, s3, out;
	char* str;
	str = "Test String";
	s = str;
	int passed = 0;

	std::ofstream file;
	file.open("log.txt", std::ios::out | std::ios::app);
	file << "\n";

	//Length Test:
	if (strlen(str) == s.length())
	{
		std::cout << "TEST 0\tLength\tPassed\n";
		file << "TEST 0\tLength\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 0\tLength\tFailed\n";
		file << "TEST 0\tLength\tFailed\n";
	}

	//Character Access Test
	if (str[5] == s[5])
	{
		std::cout << "TEST 1\tCharacter Access\tPassed\n";
		file << "TEST 1\tCharacter Access\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 1\tCharacter Access\tFailed\n";
		file << "TEST 1\tCharacter Access\tFailed\n";
	}

	//Equality Test
	s2 = "Why Hello There";
	s3 = str;

	if (!(s == s2) && s == s3)
	{
		std::cout << "TEST 2\tEquality\tPassed\n";
		file << "TEST 2\tEquality\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 2\tEquality\tFailed\n";
		file << "TEST 2\tEquality\tFailed\n";
	}

	//Append Test
	s2 = "!";
	s3 = "Test String!";
	if (s + s2 == s3)
	{
		std::cout << "TEST 3\tAppend\tPassed\n";
		file << "TEST 3\tAppend\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 3\tAppend\tFailed\n";
		file << "TEST 3\tAppend\tFailed\n";
	}

	//Prepend Test
	s2 = "!";
	s3 = "!Test String";
	if (s2 + s == s3)
	{
		std::cout << "TEST 4\tPrepend\tPassed\n";
		file << "TEST 4\tPrepend\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 4\tPrepend\tFailed\n";
		file << "TEST 4\tPrepend\tFailed\n";
	}

	//CString Test
	if (strlen(s))
	{
		std::cout << "TEST 5\tconvertToCString\tPassed\n";
		file << "TEST 5\tconvertToCString\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 5\tconvertToCString\tFailed\n";
		file << "TEST 5\tconvertToCString\tFailed\n";
	}

	//toLower Test
	s2 = "test string";
	if (s.toLower() == s2)
	{
		std::cout << "TEST 6\ttoLower\tPassed\n";
		file << "TEST 6\ttoLower\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 6\ttoLower\tFailed\n";
		file << "TEST 6\ttoLower\tFailed\n";
	}

	//toUpper Test
	s2 = "TEST STRING";
	if (s.toUpper() == s2)
	{
		std::cout << "TEST 7\ttoUpper\tPassed\n";
		file << "TEST 7\ttoUpper\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 7\ttoUpper\tFailed\n";
		file << "TEST 7\ttoUpper\tFailed\n";
	}

	//FindSubString
	s2 = "test";
	if (s.toLower().findSubstring(s2) > -1)
	{
		std::cout << "TEST 8\tfindSubstring\tPassed\n";
		file << "TEST 8\tfindSubstring\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 8\tfindSubstring\tFailed\n";
		file << "TEST 8\tfindSubstring\tFailed\n";
	}

	//FindSubString
	s2 = "string";
	if (s.toLower().findSubstring(s2, 4) > -1)
	{
		std::cout << "TEST 9\tfindSubstringFromIndex\tPassed\n";
		file << "TEST 9\tfindSubstringFromIndex\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 9\tfindSubstringFromIndex\tFailed\n";
		file << "TEST 9\tfindSubstringFromIndex\tFailed\n";
	}

	//ReplaceSubstring
	s2 = "test sting";
	if (s.toLower().replaceSubstring("string", "sting") == s2)
	{
		std::cout << "TEST 10\treplaceSubstring\tPassed\n";
		file << "TEST 10\treplaceSubstring\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 10\treplaceSubstring\tFailed\n";
		file << "TEST 10\treplaceSubstring\tFailed\n";
	}
	
	//Set To C String
	s = "";
	s = str;
	if (strcmp(s, str) == 0)
	{
		std::cout << "TEST 11\tsetToCString\tPassed\n";
		file << "TEST 11\tsetToCString\tPassed\n";
		passed++;
	}
	else
	{
		std::cout << "TEST 11\tsetToCString\tFailed\n";
		file << "TEST 11\tsetToCString\tFailed\n";
	}
	std::cout << "Passed: " << passed << "/12 tests ";
	file << "Passed: " << passed << "/12 tests ";
	std::cout << "Pass %: " << (passed / 12) * 100;
	file << "Pass %: " << (passed / 12) * 100;
	file.close();
}


int main()
{
	unitTest();
	std::cin.ignore();
    return 0;
}

