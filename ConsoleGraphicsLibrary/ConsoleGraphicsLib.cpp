// GraphicsEngDLL.cpp : Defines the exported functions for the DLL application.
//

#include "lodepng.h"
#include <thread>
#include <chrono>
#include "ConsoleGraphicsLib.h"




void GraphicsEng::runEng()
{

	DWORD numEvents = 0;
	DWORD numEventsRead = 0;

	wHnd = GetStdHandle(STD_OUTPUT_HANDLE);
	rHnd = GetStdHandle(STD_INPUT_HANDLE);
	SetConsoleWindowInfo(wHnd, TRUE, &windowSize);
	SetConsoleScreenBufferSize(wHnd, bufferSize);

	while (running)
	{
		GetNumberOfConsoleInputEvents(rHnd, &numEvents);
		if (numEvents != 0) {
			INPUT_RECORD *eventBuffer = new INPUT_RECORD[numEvents];

			ReadConsoleInput(rHnd, eventBuffer, numEvents, &numEventsRead);

			for (DWORD i = 0; i < numEventsRead; ++i) {

				if (eventBuffer[i].EventType == KEY_EVENT) {
					keys[eventBuffer[i].Event.KeyEvent.wVirtualKeyCode] = eventBuffer[i].Event.KeyEvent.bKeyDown ? keys[eventBuffer[i].Event.KeyEvent.wVirtualKeyCode] + 1 : 0;
				}
				else if (eventBuffer[i].EventType == MOUSE_EVENT)
				{
					COORD mPos = eventBuffer[i].Event.MouseEvent.dwMousePosition;
					for (ScrollBox* s : scrollBoxList)
					{
						s->update(eventBuffer[i].Event.MouseEvent.dwMousePosition, eventBuffer[i].Event.MouseEvent.dwButtonState);
					}	
				}
			}
			delete[] eventBuffer;
		}



		deltaTicks = (float)((clock() - curTime));
		curTime = clock();
		update();
		initRender();
		finishRender();
	}
}

void GraphicsEng::update()
{
	//Update Things Here
}

void GraphicsEng::initRender()
{
	clearBuffer();

	//Render Things here
	/*
	Example:
	consoleBuffer[x + y*width].Char.AsciiChar = 0x01
	consoleBuffer[x + y*width].Attributes = 0xFC
	*/



}

void GraphicsEng::finishRender()
{
	WriteConsoleOutputA(wHnd, consoleBuffer, bufferSize, bufferPos, &windowSize);
}

void GraphicsEng::clearBuffer()
{
	//Iterates through the buffer and clears all characters
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			consoleBuffer[x + width * y].Char.AsciiChar = 0x00;

			consoleBuffer[x + width * y].Attributes = 0x00;
		}
	}
}

void GraphicsEng::renderText(const char* inStr, COORD position, int colour)
{
	int len = (int)strlen(inStr);
	COORD tPos = position;
	for (int i = 0; i < len; i++)
	{
		if (inStr[i] == '\n')
		{
			tPos.Y++;
			tPos.X = position.X;
		}
		else
		{
			if (tPos.X < width && tPos.X >= 0 && tPos.Y >= 0 && tPos.Y < height)
			{
				consoleBuffer[tPos.X + width*tPos.Y].Char.AsciiChar = inStr[i];
				consoleBuffer[tPos.X + width*tPos.Y].Attributes = colour;
			}
			tPos.X++;
		}
	}

}

void GraphicsEng::renderRectangle(SMALL_RECT rectangle, int charData, int colour)
{
	for (int x = rectangle.Left; x < rectangle.Right; x++)
	{
		for (int y = rectangle.Top; y < rectangle.Bottom; y++)
		{
			if (x < width && x >= 0 && y >= 0 && y < height)
			{
				consoleBuffer[x + width*y].Char.AsciiChar = charData;
				consoleBuffer[x + width*y].Attributes = colour;
			}
		}
	}
}

void GraphicsEng::renderEllipse(SMALL_RECT bounds, int charData, int colour)
{
	COORD radii = { (bounds.Right - bounds.Left) / 2, (bounds.Bottom - bounds.Top) / 2 };
	COORD centre = { (bounds.Right + bounds.Left) / 2, (bounds.Bottom + bounds.Top) / 2 };

	for (int x = bounds.Left; x < bounds.Right; x++)
	{
		for (int y = bounds.Top; y < bounds.Bottom; y++)
		{
			if (x < width && x >= 0 && y >= 0 && y < height)
			{
				if (((((float)x - (float)centre.X) * ((float)x - (float)centre.X)) / ((float)radii.X * (float)radii.X)) + ((((float)y - (float)centre.Y) * ((float)y - (float)centre.Y)) / ((float)radii.Y * (float)radii.Y)) < 1.0f)
				{
					consoleBuffer[x + width*y].Char.AsciiChar = charData;
					consoleBuffer[x + width*y].Attributes = colour;
				}
			}
		}
	}
}

void GraphicsEng::renderSprite(Sprite sprite, COORD position)
{
	if (width + height > 0)
	{
		for (int x = position.X; x < position.X + (int)sprite.width; x++)
		{
			for (int y = position.Y; y < position.Y + (int)sprite.height; y++)
			{
				if (x > 0 && x < width && y > 0 && y < height)
				{
					float red = 0, green = 0, blue = 0, alpha = 0;
					red = sprite.pixels[(4 * (x - position.X) + 0 + 4 * sprite.width*(y - position.Y))];
					green = sprite.pixels[(4 * (x - position.X) + 1 + 4 * sprite.width*(y - position.Y))];
					blue = sprite.pixels[(4 * (x - position.X) + 2 + 4 * sprite.width*(y - position.Y))];
					alpha = sprite.pixels[(4 * (x - position.X) + 3 + 4 * sprite.width*(y - position.Y))];

					int colour = 0;
					colour += (red == 0) ? 0 : 4;
					colour += (green == 0) ? 0 : 2;
					colour += (blue == 0) ? 0 : 1;
					if (red + blue + green >= 255)
					{
						colour += 8;
					}

					if (alpha < 255 / 5)
					{
						continue;
					}
					else if (alpha < (2 * 255) / 5)
					{
						consoleBuffer[x + width*y].Char.AsciiChar = '.';
						consoleBuffer[x + width*y].Attributes = (char)colour;
					}
					else if (alpha < (3 * 255) / 5)
					{
						consoleBuffer[x + width*y].Char.AsciiChar = 'O';
						consoleBuffer[x + width*y].Attributes = (char)colour;
					}
					else if (alpha < (4 * 255) / 5)
					{
						consoleBuffer[x + width*y].Char.AsciiChar = '#';
						consoleBuffer[x + width*y].Attributes = (char)colour;
					}
					else if (alpha < 255)
					{
						consoleBuffer[x + width*y].Char.AsciiChar = '@';
						consoleBuffer[x + width*y].Attributes = (char)colour;
					}
					else if (alpha == 255)
					{
						consoleBuffer[x + width*y].Char.AsciiChar = (char)0xDB;
						consoleBuffer[x + width*y].Attributes = (char)colour;
					}
				}
			}
		}
	}
}

void GraphicsEng::renderScrollBox(ScrollBox * scrollBox)
{
	const CHAR_INFO* buf = scrollBox->getBuf();
	const COORD bufSize = scrollBox->getBufSize();
	const COORD off = scrollBox->getOff();
	SMALL_RECT internalRect = scrollBox->getRect();
	for (int x = 0; x < internalRect.Right - internalRect.Left; ++x)
	{
		for (int y = 0; y < internalRect.Bottom - internalRect.Top; ++y)
		{
			if (x == 0 || x == (internalRect.Right - internalRect.Left) - 1 || y == 0 || y == (internalRect.Bottom -internalRect.Top) - 1)
			{
				consoleBuffer[(x + internalRect.Left) + (y + internalRect.Top) * width].Char.AsciiChar = ' ';
				consoleBuffer[(x + internalRect.Left) + (y + internalRect.Top) * width].Attributes = 0xF0;
			}
			else
			{
				consoleBuffer[(x + internalRect.Left) + (y + internalRect.Top) * width] = buf[(x - 1 + off.X) + (y - 1 + off.Y) * (bufSize.X)];
			}
		}
	}
	//Bottom Left
	consoleBuffer[internalRect.Left + 1 + (internalRect.Bottom - 1)*width].Char.AsciiChar = '<';
	//Bottom Middle
	consoleBuffer[internalRect.Right - 2 + (internalRect.Bottom - 1)*width].Char.AsciiChar = '>';
	//Bottom Right
	consoleBuffer[internalRect.Right - 1 + (internalRect.Bottom - 2)*width].Char.AsciiChar = 'v';
	//Top Right
	consoleBuffer[internalRect.Right - 1 + (internalRect.Top + 1)*width].Char.AsciiChar = '^';
}

void GraphicsEng::addScrollBox(ScrollBox * box)
{
	scrollBoxList.push_back(box);
}

void GraphicsEng::removeScrollBox(ScrollBox * box)
{
	scrollBoxList.remove(box);
}

GraphicsEng::GraphicsEng(int windowWidth, int windowHeight)
{
	height = windowHeight;
	width = windowWidth;

	windowSize = { 0, 0, (SHORT)(width - 1), (SHORT)(height - 1) };
	bufferSize = { (SHORT)width, (SHORT)height };
	consoleBuffer = new CHAR_INFO[width*height]; // IMPORTANT: set length to width * height
	bufferPos = { 0, 0 };
}

GraphicsEng::~GraphicsEng()
{
	delete[] consoleBuffer;
}

Sprite GraphicsEng::loadSprite(const char * path)
{
	Sprite out;

	if (lodepng::decode(out.pixels, out.width, out.height, path))
	{
		return Sprite();
	}
	else
	{
		return out;
	}

}


//ScrollBox Functions
ScrollBox::ScrollBox(SMALL_RECT winSize, COORD bufSize)
{
	m_windowSize = winSize;
	m_bufferSize = bufSize;
	m_contentBuffer = new CHAR_INFO[m_bufferSize.X * m_bufferSize.Y];
	clearBuffer();
}

ScrollBox::~ScrollBox()
{
	delete[] m_contentBuffer;
}

void ScrollBox::clearBuffer()
{
	//Iterates through the buffer and clears all characters
	for (int y = 0; y < m_bufferSize.Y; ++y) {
		for (int x = 0; x < m_bufferSize.X; ++x) {
			m_contentBuffer[x + m_bufferSize.X * y].Char.AsciiChar = 0x00;

			m_contentBuffer[x + m_bufferSize.X * y].Attributes = 0x00;
		}
	}
}

void ScrollBox::renderText(const char* inStr, COORD position, int colour)
{
	int len = (int)strlen(inStr);
	COORD tPos = position;
	for (int i = 0; i < len; i++)
	{
		if (inStr[i] == '\n')
		{
			tPos.Y++;
			tPos.X = position.X;
		}
		else
		{
			if (tPos.X < m_bufferSize.X && tPos.X >= 0 && tPos.Y >= 0 && tPos.Y < m_bufferSize.Y)
			{
				m_contentBuffer[tPos.X + m_bufferSize.X*tPos.Y].Char.AsciiChar = inStr[i];
				m_contentBuffer[tPos.X + m_bufferSize.X*tPos.Y].Attributes = colour;
			}
			tPos.X++;
		}
	}

}

void ScrollBox::renderRectangle(SMALL_RECT rectangle, int charData, int colour)
{
	for (int x = rectangle.Left; x < rectangle.Right; x++)
	{
		for (int y = rectangle.Top; y < rectangle.Bottom; y++)
		{
			if (x < m_bufferSize.X && x >= 0 && y >= 0 && y < m_bufferSize.Y)
			{
				m_contentBuffer[x + m_bufferSize.X*y].Char.AsciiChar = charData;
				m_contentBuffer[x + m_bufferSize.X*y].Attributes = colour;
			}
		}
	}
}

void ScrollBox::renderEllipse(SMALL_RECT bounds, int charData, int colour)
{
	COORD radii = { (bounds.Right - bounds.Left) / 2, (bounds.Bottom - bounds.Top) / 2 };
	COORD centre = { (bounds.Right + bounds.Left) / 2, (bounds.Bottom + bounds.Top) / 2 };

	for (int x = bounds.Left; x < bounds.Right; x++)
	{
		for (int y = bounds.Top; y < bounds.Bottom; y++)
		{
			if (x < m_bufferSize.X && x >= 0 && y >= 0 && y < m_bufferSize.Y)
			{
				if (((((float)x - (float)centre.X) * ((float)x - (float)centre.X)) / ((float)radii.X * (float)radii.X)) + ((((float)y - (float)centre.Y) * ((float)y - (float)centre.Y)) / ((float)radii.Y * (float)radii.Y)) < 1.0f)
				{
					m_contentBuffer[x + m_bufferSize.X*y].Char.AsciiChar = charData;
					m_contentBuffer[x + m_bufferSize.X*y].Attributes = colour;
				}
			}
		}
	}
}

void ScrollBox::renderSprite(Sprite sprite, COORD position)
{
	if (m_bufferSize.X + m_bufferSize.Y > 0)
	{
		for (int x = position.X; x < position.X + (int)sprite.width; x++)
		{
			for (int y = position.Y; y < position.Y + (int)sprite.height; y++)
			{
				if (x > 0 && x < m_bufferSize.X && y > 0 && y < m_bufferSize.Y)
				{
					float red = 0, green = 0, blue = 0, alpha = 0;
					red = sprite.pixels[(4 * (x - position.X) + 0 + 4 * sprite.width*(y - position.Y))];
					green = sprite.pixels[(4 * (x - position.X) + 1 + 4 * sprite.width*(y - position.Y))];
					blue = sprite.pixels[(4 * (x - position.X) + 2 + 4 * sprite.width*(y - position.Y))];
					alpha = sprite.pixels[(4 * (x - position.X) + 3 + 4 * sprite.width*(y - position.Y))];

					int colour = 0;
					colour += (red == 0) ? 0 : 4;
					colour += (green == 0) ? 0 : 2;
					colour += (blue == 0) ? 0 : 1;
					if (red + blue + green >= 255)
					{
						colour += 8;
					}

					if (alpha < 255 / 5)
					{
						continue;
					}
					else if (alpha < (2 * 255) / 5)
					{
						m_contentBuffer[x + m_bufferSize.X*y].Char.AsciiChar = '.';
						m_contentBuffer[x + m_bufferSize.X*y].Attributes = (char)colour;
					}
					else if (alpha < (3 * 255) / 5)
					{
						m_contentBuffer[x + m_bufferSize.X*y].Char.AsciiChar = 'O';
						m_contentBuffer[x + m_bufferSize.X*y].Attributes = (char)colour;
					}
					else if (alpha < (4 * 255) / 5)
					{
						m_contentBuffer[x + m_bufferSize.X*y].Char.AsciiChar = '#';
						m_contentBuffer[x + m_bufferSize.X*y].Attributes = (char)colour;
					}
					else if (alpha < 255)
					{
						m_contentBuffer[x + m_bufferSize.X*y].Char.AsciiChar = '@';
						m_contentBuffer[x + m_bufferSize.X*y].Attributes = (char)colour;
					}
					else if (alpha == 255)
					{
						m_contentBuffer[x + m_bufferSize.X*y].Char.AsciiChar = (char)0xDB;
						m_contentBuffer[x + m_bufferSize.X*y].Attributes = (char)colour;
					}
				}
			}
		}
	}
}

void ScrollBox::setPos(COORD pos)
{
	COORD dims = { m_windowSize.Right - m_windowSize.Left, m_windowSize.Bottom - m_windowSize.Top };
	m_windowSize.Left = pos.X;
	m_windowSize.Right = pos.X + dims.X;
	m_windowSize.Top = pos.Y;
	m_windowSize.Bottom = pos.Y + dims.Y;
}

void ScrollBox::update(COORD mPos, DWORD mState)
{
	if (mPos.X == m_windowSize.Right - 1 && mPos.Y == m_windowSize.Bottom - 2) //Scroll Down (Bottom Right Corner)
	{
		if (mState == FROM_LEFT_1ST_BUTTON_PRESSED && mState != m_prevState)
		{
			m_offset = { m_offset.X, min(m_offset.Y + 1, m_bufferSize.Y - (m_windowSize.Bottom - m_windowSize.Top - 2)) };

		}
	}
	if (mPos.X == m_windowSize.Right - 1 && mPos.Y == m_windowSize.Top + 1) //Scroll Up (Top Right Corner)
	{
		if (mState == FROM_LEFT_1ST_BUTTON_PRESSED && mState != m_prevState)
		{
			m_offset = { m_offset.X, max(m_offset.Y - 1,0) };
		}
	}
	if (mPos.X == m_windowSize.Left + 1 && mPos.Y == m_windowSize.Bottom - 1) //Scroll Left (Bottom Left Corner)
	{
		if (mState == FROM_LEFT_1ST_BUTTON_PRESSED && mState != m_prevState)
		{
			m_offset = { max(m_offset.X - 1,0) , m_offset.Y };
		}
		
	}
	if (mPos.X == m_windowSize.Right - 2 && mPos.Y == m_windowSize.Bottom - 1) //Scroll Right (Bottom Right Corner)
	{
		if (mState == FROM_LEFT_1ST_BUTTON_PRESSED && mState != m_prevState)
		{
			m_offset = { min(m_offset.X + 1, m_bufferSize.X - (m_windowSize.Right - m_windowSize.Left - 2)), m_offset.Y };
		}
		
	}
}

void ScrollBox::scroll(COORD offset)
{
	m_offset = { max(min(m_offset.X + offset.X ,m_bufferSize.X - (m_windowSize.Right - m_windowSize.Left - 2)), 0), max(min(m_offset.Y - offset.Y, m_bufferSize.Y - (m_windowSize.Bottom - m_windowSize.Top - 2)), 0) };
}

const CHAR_INFO * ScrollBox::getBuf()
{
	return m_contentBuffer;
}

const SMALL_RECT ScrollBox::getRect()
{
	return m_windowSize;
}

const COORD ScrollBox::getOff()
{
	return m_offset;
}

const COORD ScrollBox::getBufSize()
{
	return m_bufferSize;
}
