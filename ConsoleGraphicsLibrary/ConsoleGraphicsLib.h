#pragma once

#include <Windows.h>
#include <vector>
#include <list>
#include <time.h>

#define PI 3.1415926536

class GraphicsEng;

struct Sprite
{
	std::vector<unsigned char> pixels;
	unsigned width = 0, height = 0;
};

class ScrollBox
{
public:

	ScrollBox(SMALL_RECT windowSize, COORD bufferSize);
	~ScrollBox();
	void clearBuffer();
	void renderText(const char* inStr, COORD position, int colour);
	void renderRectangle(SMALL_RECT rectangle, int charData, int colour);
	void renderEllipse(SMALL_RECT bounds, int charData, int colour);
	void renderSprite(Sprite sprite, COORD position);
	void setPos(COORD pos);
	void update(COORD mPos, DWORD mState);
	void scroll(COORD offset);
	
	
	const CHAR_INFO* getBuf();
	const SMALL_RECT getRect();
	const COORD getOff();
	const COORD getBufSize();
private:
	CHAR_INFO* m_contentBuffer;
	SMALL_RECT m_windowSize;
	COORD m_offset, m_bufferSize;
	SHORT m_prevState;
};


class GraphicsEng
{
public:


	GraphicsEng(int windowWidth = 100, int windowHeight = 50);
	~GraphicsEng();

	void runEng();
	virtual void update();
	virtual void initRender();
	void clearBuffer();
	void renderText(const char* inStr, COORD position, int colour);
	void renderRectangle(SMALL_RECT rectangle, int charData, int colour);
	void renderEllipse(SMALL_RECT bounds, int charData, int colour);
	void renderSprite(Sprite sprite, COORD position);
	void renderScrollBox(ScrollBox* scrollBox);
	void addScrollBox(ScrollBox* box);
	void removeScrollBox(ScrollBox* box);
	Sprite loadSprite(const char* path);

	const unsigned* GetKeyState() { return keys; }
	const float getDeltaTicks() { return deltaTicks; }
	const bool toggleRunning() { running = !running; return running; }

private:
	void finishRender();
	HANDLE wHnd, rHnd;

	int height;
	int width;

	bool running = true;

	SMALL_RECT windowSize;
	COORD bufferSize;
	CHAR_INFO* consoleBuffer; // IMPORTANT: set length to width * height
	std::list<ScrollBox*> scrollBoxList;
	COORD bufferPos;


	float deltaTicks = 0;
	clock_t curTime = 0;

	unsigned keys[256];

};