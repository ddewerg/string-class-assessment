#pragma once
#include "ConsoleGraphicsLib.h"
#include "Items.h"
#include "Character.h"
#include <map>
#include "StrLib.h"
#include <vector>
#include <list>

struct StrCompare : public std::binary_function<const char*, const char*, bool> {
public:
	bool operator() (const char* str1, const char* str2) const
	{
		return std::strcmp(str1, str2) < 0;
	}
}; //Code found on stack overflow (Comparison function for const char* for use in std::map)

typedef std::map<const char*, Item*, StrCompare> ItemTable;
typedef std::list<Character*>::iterator enemyIter;
typedef std::list<ItemStack*>::iterator iStackIter;
typedef std::map<const char*, Stat> StatTable;



struct Location
{
	String name = "";
	String path = "";
	bool isLocked = false;
	String lockItem = "";
};


struct Level
{
	std::list<Character*> enemyList;
	std::vector<Location> linkedLocations;
	std::list<ItemStack*> items;
	String description = "";
	String selfPath = "";
};

class CEng : public GraphicsEng
{
public:

	void updateEnemies();
	void printToLog(String strToPrint);
	virtual void update();
	virtual void initRender();
	void handleLevelSwap(Location loc);
	void handleKeys();
	void handleCommand(Level * currentLevel);
	Level * loadLevel(const char* path);
	void renderLevel(Level * l);
	ItemTable* getItemTablePointer() { return &itemTable; }

private:
	ItemTable itemTable;
};



