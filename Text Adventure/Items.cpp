
#include "Items.h"

void Potion::use(Character & target)
{
	target.changeHealth((float)magnitude);
}

void BuffPotion::use(Character & target)
{
	const int* targStats = target.getStats();
	int* tmpStats = new int[Stat::STAT_COUNT];
	for (int i = 0; i < Stat::STAT_COUNT; ++i)
	{
		tmpStats[i] = targStats[i];
	}
	tmpStats[statToBuff] += magnitude;
	target.setStats(tmpStats[Stat::HP], tmpStats[Stat::ATTACK], tmpStats[Stat::DEFENSE], tmpStats[Stat::SPEED]);
}