#include "Items.h"
#include "StrLib.h"
#include "Text_Adventure.h"
#include <list>
#include <algorithm>
#include "Character.h"


Character::~Character()
{
	for (ItemStack* i : m_items)
	{
		delete i;
	}
}

String Character::attack(Character& target)
{
	String returnStr;
	if (m_equip != nullptr)
	{
		if (Potion* pot = dynamic_cast<Potion*>(m_equip))
		{
			returnStr = m_name + " used " + pot->name + " on " + target.getName();
			pot->use(target);
			m_equip = nullptr;
		}
		else if (Weapon* weap = dynamic_cast<Weapon*>(m_equip))
		{
			char buf[10];
			float dmg = -(2.0f + (float)(weap->damage) / 3.0f) * (float)m_stats[Stat::ATTACK] * ((float)m_stats[Stat::SPEED] / 10.0f) * (1.0f / ((float)target.m_stats[Stat::DEFENSE] + 1.0f));
			sprintf_s(buf, "%f", -dmg);
			returnStr = m_name + " attacked " + target.getName() + " with " + weap->name + " dealing " + buf + " damage";
			target.changeHealth(dmg);
		}
		else
		{
			char buf[10];
			float dmg = -2.0f * (float)m_stats[Stat::ATTACK] * ((float)m_stats[Stat::SPEED] / 10.0f) * (1.0f / ((float)target.m_stats[Stat::DEFENSE] + 1.0f));
			sprintf_s(buf, "%f", -dmg);
			returnStr = m_name + " attacked " + target.getName() + " dealing " + buf + " damage";
			target.changeHealth(dmg);
		}
	}
	else
	{
		char buf[10];
		float dmg = -2.0f * (float)m_stats[Stat::ATTACK] * ((float)m_stats[Stat::SPEED] / 10.0f) * (1.0f / ((float)target.m_stats[Stat::DEFENSE] + 1.0f));
		sprintf_s(buf, "%f", -dmg);
		returnStr = m_name + " attacked " + target.getName() + " dealing " + buf + " damage";
		target.changeHealth(dmg);
	}
	return returnStr;
}

void Character::equipItem(String item, CEng& eng)
{
	if (m_equip != nullptr)
	{
		unequipItem(eng);
	}
	if (takeItem(item))
	{
		auto itemIter = eng.getItemTablePointer()->find(item);
		if (itemIter != eng.getItemTablePointer()->end())
		{
			// VALID object in map
			eng.printToLog((String)"Equipped " + itemIter->second->name);
			m_equip = itemIter->second;
		}
	}
}

void Character::unequipItem(CEng& eng)
{
	if (m_equip != nullptr)
	{
		eng.printToLog((String)"Returned " + m_equip->name + " to inventory");
		giveItem(m_equip->name, eng);
		m_equip = nullptr;
	}
}

void Character::setStats(int hp, int attack, int defense, int speed)
{
	m_stats[Stat::HP] = hp;
	m_curHealth = (float)hp;
	m_stats[Stat::ATTACK] = attack;
	m_stats[Stat::DEFENSE] = defense;
	m_stats[Stat::SPEED] = speed;
}

const int * Character::getStats()
{
	return m_stats;
}

void Character::changeHealth(float amount)
{
	m_curHealth = std::max<int>(0, std::min<int>(m_curHealth + amount, m_stats[Stat::HP]));
}

float Character::getHP()
{
	return m_curHealth;
}

void Character::usePotion(Potion * potion)
{
	potion->use(*this);
	takeItem(potion->name);
}

void Character::giveItem(String item, CEng& eng, int number)
{
	bool hasItem = false;
	for (ItemStack* i : m_items)
	{
		if (i->item->name == item)
		{
			i->count += number;
			hasItem = true;
			break;
		}
	}
	if (!hasItem)
	{
		ItemStack* i = new ItemStack();
		i->count = number;
		auto itemIter = eng.getItemTablePointer()->find(item);
		if (itemIter != eng.getItemTablePointer()->end())
		{
			// VALID object in map
			i->item = itemIter->second;
			m_items.push_back(i);
		}

	}
}

bool Character::takeItem(String item)
{
	bool hasItem = false;
	for (ItemStack* i : m_items)
	{
		if (i->item->name == item)
		{
			if (i->count > 1)
			{
				i->count--;
			}
			else
			{
				m_items.remove(i);
			}
			hasItem = true;
			break;
		}
	}
	return hasItem;
}

const std::list<ItemStack*> Character::viewInv()
{
	return m_items;
}

bool Character::isDead()
{
	return m_curHealth <= 0;
}

bool Character::operator==(Character other)
{
	return m_name.operator==(other.m_name);
}