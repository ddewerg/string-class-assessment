// Text Adventure.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <vector>
#include <iostream>
#include <ConsoleGraphicsLib.h>
#include <StrLib.h>
#include "Text Adventure.h"

CEng cEng;

int main()
{
	cEng.runEng();
    return 0;
}

Level loadLevel(const char * path)
{
	return Level();
}

void CEng::update()
{
	String command;
	char* cmd;
	std::cin >> cmd;
	command = cmd;
	delete[] cmd;
	system("CLS");

	GraphicsEng::update();
}

void CEng::initRender()
{
	GraphicsEng::initRender();
}
