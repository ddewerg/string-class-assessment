#pragma once
#include "StrLib.h"
#include "Character.h"

struct Item
{
	String name = "";
	int value;
	virtual ~Item() {}
};

struct Potion : Item
{
	int magnitude;
	virtual void use(Character& target);
};

struct BuffPotion : Potion
{
	Stat statToBuff;
	virtual void use(Character& target);
};

struct ItemStack
{
	Item* item;
	int count = 0;
};

struct Weapon : Item
{
	int damage;
	bool ranged;
};