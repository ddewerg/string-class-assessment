// Text Adventure.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <fstream>
#include <iostream>
#include <iostream>
#include <cstring>
#include <functional>
#include "Text_Adventure.h"


std::vector<Level*> staticLevels;
CEng cEng;
Level * curLevel;
Character player;
bool attacked = false;
ScrollBox* eventLog;
int linesUsed;
String command, errStr;
unsigned prevKeys[256];




int main()
{
	char* n = new char[100];
	std::cout << "Your name: ";
	std::cin >> n;
	std::cout << std::endl;
	player.setName(n);
	delete[] n;
	player.setStats(100, 10, 15, 20);
	Weapon* bSwd = new Weapon();
	bSwd->damage = 5;
	bSwd->ranged = false;
	bSwd->name = "Bronze Sword";
	bSwd->value = 10;
	(*(cEng.getItemTablePointer()))["Bronze Sword"] = bSwd;

	player.giveItem("Bronze Sword", cEng);
	
	eventLog = new ScrollBox({ 0, 39, 99, 47 }, { 98, 1000 });
	player.equipItem("Bronze Sword", cEng);
	cEng.addScrollBox(eventLog);
	curLevel = cEng.loadLevel("..\\Levels\\Start.txt");
	cEng.runEng();
	return 0;
}

void CEng::updateEnemies()
{
	if (attacked == false)
	{
		for (Character* e : curLevel->enemyList)
		{
			printToLog(e->attack(player));
			if (player.isDead())
			{
				curLevel = loadLevel("..\\Levels\\GameOver.txt");
			}
		}
		attacked = true;
	}
}

void CEng::printToLog(String strToPrint)
{
	if (linesUsed > 996)
	{
		eventLog->scroll({ 0, (SHORT)linesUsed });
		linesUsed = 0;
		eventLog->clearBuffer();
	}
	eventLog->renderText(strToPrint, { 0, (SHORT)linesUsed }, 0x0A);
	linesUsed += 2;
	if(linesUsed > 6 ) eventLog->scroll({ 0, -2 });
		
}

Level * CEng::loadLevel(const char * path)
{
	for (Level * lvl : staticLevels)
	{
		if (lvl->selfPath.operator==((String)path))
		{
			return lvl;
		}
	}
	Level* l = new Level();
	l->selfPath = path;
	std::ifstream levelStream(path);
	if (levelStream.good())
	{

		char* str = new char[101];
		do
		{
			levelStream.getline(str, 100);
			if (str[0] == '#')
			{
				String s;
				s = str;
				if (s.findSubstring("\\\\") == -1)
				{
					if (s.findSubstring("#static") > -1)
					{
						staticLevels.push_back(l);
					}
					if (s.findSubstring("#location") > -1)
					{
						Location loc;
						std::vector<String> sv = s.split("|", 10); //10 = "#location " length
						loc.name = sv[0];
						loc.path = sv[1];
						if (sv.size() == 3)
						{
							loc.isLocked = true;
							loc.lockItem = sv[2];
						}
						l->linkedLocations.push_back(loc);
					}
					if (s.findSubstring("#enemy") > -1)
					{
						Character* enemy = new Character();
						std::vector<String> sv = s.split("|", 7); //7 = "#enemy " length
						if (sv.size() < 5) continue;
						int eCount = 0;
						for (Character* e : l->enemyList)
						{
							if (e->getName() == sv[0])
							{
								eCount++;
							}
							if (eCount > 0)
							{
								char buf[3];
								sprintf_s(buf, "%i", eCount);
								sv[0] += (String)" " + buf;
							}
						}
						enemy->setName(sv[0]);
						enemy->setStats(atoi(sv[1]), atoi(sv[2]), atoi(sv[3]), atoi(sv[4]));
						for (unsigned i = 5; i < sv.size(); ++i)
						{
							if (itemTable.at(sv[i])->name != "")
							{
								enemy->giveItem(sv[i], cEng);
							}
						}
						l->enemyList.push_back(enemy);
					}
					if (s.findSubstring("#item") > -1)
					{
						std::vector<String> sv = s.split("|", 6); //6 = "#item " length
						bool nameFound = false;
						for (std::pair<String, Item*> p : itemTable)
						{
							if (p.first == sv[1])
							{
								nameFound = true;
								break;
							}
						}

						if (!nameFound)
						{
							Item* item;
							if (sv[0].toLower().operator==((String)"potion"))
							{
								item = new Potion();
								((Potion*)item)->name = sv[1];
								((Potion*)item)->value = atoi(sv[2]);
								((Potion*)item)->magnitude = atoi(sv[3]);
							}
							else if (sv[0].toLower().operator==((String)"buffpotion"))
							{
								item = new BuffPotion();
								((BuffPotion*)item)->name = sv[1];
								((BuffPotion*)item)->value = atoi(sv[2]);
								((BuffPotion*)item)->magnitude = atoi(sv[4]);
								
								if (sv[3].toUpper().operator==((String)"ATK"))
								{
									((BuffPotion*)item)->statToBuff = Stat::ATTACK;
								}
								else if (sv[3].toUpper().operator==((String)"DEF"))
								{
									((BuffPotion*)item)->statToBuff = Stat::DEFENSE;
								}
								else if (sv[3].toUpper().operator==((String)"HP"))
								{
									((BuffPotion*)item)->statToBuff = Stat::HP;
								}
								else if (sv[3].toUpper().operator==((String)"SPD"))
								{
									((BuffPotion*)item)->statToBuff = Stat::SPEED;
								}
								
							}
							else if (sv[0].toLower().operator==((String)"weapon"))
							{
								item = new Weapon();
								((Weapon*)item)->name = sv[1];
								((Weapon*)item)->value = atoi(sv[2]);
								((Weapon*)item)->damage = atoi(sv[3]);
								if (sv[4].toLower().operator==((String)"true"))
								{
									((Weapon*)item)->ranged = true;
								}
								else
								{
									((Weapon*)item)->ranged = false;
								}
							}
							else
							{
								item = new Item();
								item->name = sv[1];
								item->value = atoi(sv[2]);
							}

							itemTable[item->name] = item;
						}

					}
					if (s.findSubstring("#loot") > -1)
					{
						std::vector<String> sv = s.split("|", 6); //7 = "#loot " length
						if (sv.size() == 2)
						{
							ItemStack* i = new ItemStack();
							auto itemIter = itemTable.find(sv[0]);
							if (itemIter != itemTable.end())
							{
								// VALID object in map
								i->item = itemIter->second;
								i->count = atoi(sv[1]);
								l->items.push_back(i);
							}

						}
					}
				}
			}
			else
			{
				String s = str;
				l->description += s + "\n";
			}
		} while (!levelStream.eof());

	}
	return l;
}

void CEng::renderLevel(Level* l)
{
	cEng.renderText(l->description, { 0,0 }, 0x0F);

	cEng.renderText("Enemies:", { 0, 19 }, 0x09);
	int x = 0;
	for (enemyIter i = l->enemyList.begin(); i != l->enemyList.end(); ++i, ++x)
	{
		if (x % 2 == 1)
		{
			cEng.renderText((*i)->getName(), { 49, (SHORT)(20 + x) }, 0x0C);
		}
		else
		{
			cEng.renderText((*i)->getName(), { 0, (SHORT)(20 + x) }, 0x0C);
		}
	}
	cEng.renderText("Items:", { 0, 24 }, 0x09);
	x = 0;
	for (iStackIter i = l->items.begin(); i != l->items.end(); ++i, ++x)
	{
		if (x % 2 == 1)
		{
			cEng.renderText((*i)->item->name, { 49, (SHORT)(25 + x) }, 0x0C);
		}
		else
		{
			cEng.renderText((*i)->item->name, { 0, (SHORT)(25 + x) }, 0x0C);
		}
	}
	cEng.renderText("Possible Locations:", { 0, 29 }, 0x09);
	for (int i = 0; i < (int)l->linkedLocations.size(); i++)
	{
		if (i % 2 == 1)
		{
			cEng.renderText(l->linkedLocations[i].name, { 49, (SHORT)(30 + i) }, 0x0C);
		}
		else
		{
			cEng.renderText(l->linkedLocations[i].name, { 0, (SHORT)(30 + i) }, 0x0C);
		}
	}
}

void CEng::update()
{
	handleKeys();
	updateEnemies();
	GraphicsEng::update();
}

void CEng::initRender()
{
	GraphicsEng::initRender();
	renderLevel(curLevel);
	renderText(errStr, {0, 48}, 0xF0);
	renderScrollBox(eventLog);
	renderText((command + "|"), { 0, 49 }, 0x0F);
}

void CEng::handleKeys()
{
	//Handle characters A-Z a-z
	for (char i = 'a'; i <= 'z'; i++)
	{
		if ((GetKeyState()[i] > prevKeys[i]) || (GetKeyState()[i - 32] > prevKeys[i - 32]))
		{
			char cmd[2] = { i , 0 };
			command += cmd;
		}
	}


	//Handle characters 0-9
	for (char i = '0'; i <= '9'; i++)
	{
		if (GetKeyState()[i] > prevKeys[i])
		{
			char cmd[2] = { i , 0 };
			command += cmd;
		}
	}

	//Handle " "
	if (GetKeyState()[32] > prevKeys[32])
	{
		command += " ";
	}

	//Handle enter
	if (GetKeyState()[13] > prevKeys[13])
	{
		handleCommand(curLevel);
	}

	//Handle delete/backspace
	if ((GetKeyState()[VK_DELETE] > prevKeys[VK_DELETE]) || (GetKeyState()['\b'] > prevKeys['\b']))
	{
		if (command.length() > 1)
		{
			String tmp;
			for (int i = 0; i < command.length() - 1; i++)
			{
				char tstr[2] = { command[i], 0 };
				tmp += tstr;
			}
			command = tmp;
		}
		else
		{
			command = "";
		}
	}


	for (int i = 0; i < 256; i++)
	{
		prevKeys[i] = GetKeyState()[i];
	}
}

void CEng::handleLevelSwap(Location loc)
{
	if (!loc.isLocked)
	{
		curLevel = loadLevel(loc.path);
		command = "";
		return;
	}
	else
	{
		bool hasKey = false;
		for (ItemStack* i : player.viewInv())
		{
			if (i->item->name == loc.lockItem)
			{
				hasKey = true;
				break;
			}
		}
		if (hasKey)
		{
			printToLog((String)"You opened the door using the " + loc.lockItem + "!");
			curLevel = loadLevel(loc.path);
		}
		else
		{
			errStr = "Door is locked, find the required item to open it!";
			
		}
		command = "";
		return;
	}
}

void CEng::handleCommand(Level * currentLevel)
{
	errStr = "";
	command = command.replaceSubstring(" ", "");
	if (command.findSubstring("moveto") > -1)
	{
		if (currentLevel->enemyList.size() == 0)
		{
			for (Location loc : currentLevel->linkedLocations)
			{
				if (command.replaceSubstring("moveto", "").operator ==(loc.name.toLower().replaceSubstring(" ", "")))
				{
					handleLevelSwap(loc);
					return;
				}
			}
		errStr = "ERROR: Invalid Location";
		}
		else
		{
			errStr = "You cant leave while monsters are here!";
		}
	}
	else if (command.findSubstring("goto") > -1)
	{
		if (currentLevel->enemyList.size() == 0)
		{
			for (Location loc : currentLevel->linkedLocations)
			{
				if (command.replaceSubstring("goto", "").operator ==(loc.name.toLower().replaceSubstring(" ", "")))
				{
					handleLevelSwap(loc);
					return;
				}
			}
			errStr = "ERROR: Invalid Location";
		}
		else
		{
			errStr = "You cant leave while monsters are here!";
		}
	}
	else if (command.findSubstring("go", 0, 2) > -1)
	{
		if (currentLevel->enemyList.size() == 0)
		{
			for (Location loc : currentLevel->linkedLocations)
			{
				if (command.replaceSubstring("go", "").operator ==(loc.name.toLower().replaceSubstring(" ", "")))
				{
					handleLevelSwap(loc);
					return;
				}
			}
			errStr = "ERROR: Invalid Location";
		}
		else
		{
			errStr = "You cant leave while monsters are here!";
		}
	}
	else if (command.findSubstring("attack", 0, 8) > -1)
	{
		for (Character* e : curLevel->enemyList)
		{
			if (command.replaceSubstring("attack", "").operator ==(e->getName().toLower().replaceSubstring(" ", "")))
			{
				printToLog(player.attack(*e));
				if (e->isDead())
				{
					for (ItemStack* i : e->viewInv())
					{
						player.giveItem(i->item->name, cEng, i->count);
						char buf[10];
						printToLog((String)"Looted: " + (String)itoa(i->count, buf, 10) + "x " + i->item->name);
					}
					printToLog(e->getName() + " died");
					curLevel->enemyList.remove(e);
					delete e;
				}
				attacked = false;
				command = "";
				return;
			}
			else
			{
				errStr = "Invalid Enemy Name!";
			}
		}
	}
	else if (command.findSubstring("pickup", 0, 8) > -1)
	{
		for (ItemStack* i : curLevel->items)
		{
			if (command.replaceSubstring("pickup", "").operator ==(i->item->name.toLower().replaceSubstring(" ", "")))
			{
				printToLog((String)"You picked up a " + i->item->name);
				if (i->count > 1)
				{
					i->count--;
				}
				else
				{
					curLevel->items.remove(i);
				}
				player.giveItem(i->item->name, cEng, i->count);
				attacked = false;
				command = "";
				return;
			}
			else
			{
				errStr = "Invalid Item Name!";
			}
		}
	}
	else if (command.findSubstring("use", 0, 5) > -1)
	{
		for (ItemStack* i : player.viewInv())
		{
			if (command.replaceSubstring("use", "").operator ==(i->item->name.toLower().replaceSubstring(" ", "")))
			{
				if (Potion* p = dynamic_cast<Potion*>(i->item))
				{
					p->use(player);
					printToLog((String)"You used a " + i->item->name);
					player.takeItem(p->name);
				}
				else
				{
					errStr = "You can't use that!";
				}
				attacked = false;
				command = "";
				return;
			}
			else
			{
				errStr = "Invalid Item Name!";
			}
		}
	}
	else if (command.findSubstring("equip", 0, 7) > -1)
	{
		for (ItemStack* i : player.viewInv())
		{
			if (command.replaceSubstring("equip", "").operator ==(i->item->name.toLower().replaceSubstring(" ", "")))
			{
				player.equipItem(i->item->name, cEng);
				attacked = false;
				command = "";
				return;
			}
			else
			{
				errStr = "Invalid Item Name!";
			}
		}
	}
	else if (command.findSubstring("status", 0, 7) > -1)
	{
		const int* stats  = player.getStats();
		if (player.getHP() == stats[0])
		{
			printToLog("You feel completely healthy!");
		}
		else if (player.getHP() > stats[0] * 0.5f)
		{
			printToLog("You feel pretty good!");
		}
		else if (player.getHP() > stats[0] * 0.1f)
		{
			printToLog("You feel bad!");
		}
		else
		{
			printToLog("You are almost dead!");
		}
	}
	else if (command.findSubstring("inventory", 0, 11) > -1)
	{
		printToLog("You have in your bag: ");
		for (ItemStack* i : player.viewInv())
		{
			char buf[10];
			printToLog((String)itoa(i->count, buf, 10) + "x " + i->item->name);
		}
	}
	else if (command.findSubstring("help", 0, 5) > -1)
	{
		printToLog("move to, go to, go <Location>: move to the given location");
		printToLog("attack <Enemy>: attack given enemy");
		printToLog("pickup <Item>: pick up the given item");
		printToLog("use <Item>: uses given item from inventory");
		printToLog("status: check your health");
		printToLog("inventory: check the items in inventory");
		printToLog("equip <Item>: equips the given item");
	}
	else
	{
		errStr = "ERROR: Invalid Command";
	}
	

	command = "";
}










