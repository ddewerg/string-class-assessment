#pragma once
#include <list>

struct ItemStack;
struct Potion;
struct Item;
class CEng;

enum Stat : int
{
	HP,
	ATTACK,
	DEFENSE,
	SPEED,
	STAT_COUNT
};

class Character
{
public:
	~Character();
	String attack(Character& target);
	void setName(String name) { m_name = name; }
	String getName() { return m_name; }
	void setStats(int hp, int attack, int defense, int speed);
	const int* getStats();
	void changeHealth(float amount);
	float getHP();
	void usePotion(Potion* potion);
	void giveItem(String item, CEng& eng, int number = 1);
	bool takeItem(String item);
	void equipItem(String item, CEng& eng);
	void unequipItem(CEng& eng);
	const std::list<ItemStack*> viewInv();
	bool isDead();
	bool operator==(Character other);

private:
	String m_name = "";
	int m_stats[Stat::STAT_COUNT];
	float m_curHealth;
	std::list<ItemStack*> m_items;
	Item* m_equip;

};

class Character;