#include "StrLib.h"
#include <String.h>


String::String(const String & string)
{
	*this = string.charArray;
}

String::String(const char * string)
{
	*this = string;
}

String::String()
{
	
}

bool String::operator==(String & other)
{
	if (length() == other.length())
	{
		for (int i = 0; i < length(); i++)
		{
			if (charArray[i] != other[i])
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
}

String String::operator+(String other)
{
	String s = *this + (const char*)other;
	return s;
}

String& String::operator+=(String other)
{
	*this = *this + other;
	return *this;
}

String String::operator+(const char * other)
{
	String s;
	s.charArray = new char[length() + strlen(other) + 1];
	s.charArray[0] = 0;
	s.isInitialised = true;
	strcat_s(s.charArray, length() + strlen(other) + 1, *this);
	strcat_s(s.charArray, length() + strlen(other) + 1, other);
	return s;
}

String& String::operator+=(const char * other)
{
	*this = *this + other;
	return *this;
}

String& String::operator=(const char * other)
{
	if (isInitialised)
	{
		delete[] charArray;
	}
	charArray = new char[strlen(other) + 1];
	strcpy_s(charArray, strlen(other) + 1, other);
	isInitialised = true;
	return *this;
}

String & String::operator=(String other)
{
	*this = other.charArray;
	return *this;
}

char String::operator[](int i)
{
	if (isInitialised)
	{
		if ((i > length()) | (i < 0))
		{
			return 0;
		}
		else
		{
			return charArray[i];
		}
	}
	else return 0;
}

String::operator const char*() const
{
	if (isInitialised)
	{
		return charArray;
	}
	else
	{
		return "";
	}
}

int String::findSubstring(const char * stringToFind, int startingIndex, int endingIndex)
{
	if (strlen(stringToFind) == 0)
	{
		return -1;
	}
	if (startingIndex > length())
	{
		return -1;
	}
	if (endingIndex == -1)
	{
		int foundIndex = -1;
		for (int j = startingIndex; j < length(); j++)
		{
			if (stringToFind[0] == charArray[j])
			{
				foundIndex = j;
			}
			if (foundIndex > -1)
			{
				for (int i = 0; i < (int)strlen(stringToFind); i++)
				{
					if (!(charArray[foundIndex + i] == stringToFind[i]))
					{
						foundIndex = -1;
						break;
					}
				}
				if (foundIndex > -1)
				{
					break;
				}
			}

		}
		return foundIndex;
	}
	else
	{
		if (startingIndex > endingIndex)
		{
			return -1;
		}
		else
		{
			
			int foundIndex = -1;
			for (int j = startingIndex; j < endingIndex; j++)
			{
				if (stringToFind[0] == charArray[j])
				{
					foundIndex = j;
				}
				if (foundIndex > -1)
				{
					for (int i = 0; i < (int)strlen(stringToFind); i++)
					{
						if (!(charArray[foundIndex + i] == stringToFind[i]))
						{
							foundIndex = -1;
							break;
						}
					}
					if (foundIndex > -1)
					{
						break;
					}
				}

			}
			return foundIndex;
		}
	}
}

String String::replaceSubstring(const char * stringToFind, const char * replacementString, int startingIndex, int endingIndex)
{
	std::vector<String> str;
	String res;
	res = "";
	if (findSubstring(stringToFind, startingIndex, endingIndex) > -1)
	{
		str = split(stringToFind);
		for (int i = 0; i < (int)str.size() - 1; i++)
		{
			res += str[i] + replacementString;
		}
		res += str[str.size() - 1];
		return res;
	}
	else
	{
		return *this;
	}
}

std::vector<String> String::split(const char * splitter, int startingIndex, int endingIndex)
{
	int numStrings = 0;
	std::vector<String> str;
	int ind = startingIndex;
	while ((ind = findSubstring(splitter, ind, endingIndex)) > -1)
	{
		numStrings += 1;
		ind += (int)strlen(splitter);
	}
	if (numStrings == 0)
	{
		str = std::vector<String>(1);
		str[0] = *this;
		return str;
	}
	else
	{
		ind = startingIndex;
		int prevInd = ind;
		str = std::vector<String>(numStrings + 1);
		for (int i = 0; i < numStrings; i++)
		{
			prevInd = ind;
			ind = findSubstring(splitter, ind, endingIndex);
			char* cstr = new char[(ind - prevInd) + 1];

			for (int j = prevInd; j < ind; j++)
			{
				cstr[j - prevInd] = (this->charArray[j]);
			}
			cstr[ind - prevInd] = 0;
			str[i] = cstr;
			ind += (int)strlen(splitter);
		}

		prevInd = ind;
		
		ind = length();

		char* cstr = new char[(ind - prevInd) + 1];

		for (int j = prevInd; j < ind; j++)
		{
			cstr[j - prevInd] = (this->charArray[j]);
		}
		cstr[ind - prevInd] = 0;
		str[numStrings] = cstr;
		
	}

	return str;
}

String String::toLower()
{
	String str = *this;

	for (int i = 0; i < length(); i++)
	{
		if (str[i] <= 'Z' && str[i] >= 'A')
		{
			str.charArray[i] += (char)32;
		}
	}
	return str;
}

String String::toUpper()
{
	String str = *this;
	
	for (int i = 0; i < length(); i++)
	{
		if (str[i] <= 'z' && str[i] >= 'a')
		{
			str.charArray[i] -= (char)32;
		}
	}
	return str;
}

int String::length()
{
	if (isInitialised)
	{
		return (int)strlen(charArray);
	}
	else
	{
		return 0;
	}
}

String::~String()
{
	if (isInitialised)
	{
		delete[] charArray;
		isInitialised = false;
	}
}
