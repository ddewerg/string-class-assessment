#pragma once

#include <vector>

class String
{
public:

	String(const String& string); //copy constructor
	String(const char* string);
	String(); //default constructor

	bool operator == (String & other); //equality check
	String operator + (String other); //concatenation
	String& operator += (String other); //append
	String operator + (const char * other); //concatenation with c string
	String& operator += (const char * other); //append with c string
	String& operator = (const char * other); //assign from c string
	String& operator = (String other); //assign from string
	char operator [] (int i); //accessing character at a given index
	operator const char * () const; //implicit conversion operator to c string
	
	int findSubstring(const char * stringToFind, int startingIndex = 0, int endingIndex = -1); //finds a substring within a given range within a string
																							   //returns -1 for no substring found (or an error) or the index of the string if it is found
	String replaceSubstring(const char * stringToFind, const char* replacementString, int startingIndex = 0, int endingIndex = -1); //replaces substring within string with another substring,
																																	//returns the original string if the inital substring isn't found
	std::vector<String> split(const char * splitter, int startingIndex = 0, int endingIndex = -1); //splits a string by a substring, returning a vector of strings divided from the original string
	String toLower(); //converts the string to lowercase
	String toUpper(); //converts the string to uppercase


	int length(); //returns the length of the string as an integer (null terminator not included in length count)


	~String(); //destructor
private:

	char* charArray = nullptr;
	bool isInitialised = false;
};