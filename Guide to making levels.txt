#static makes a level static (Enemies and items wont refresh)
#location <Name>|<Path>|<LockItem>(Optional)
#enemy <Name>|<HP>|<ATK>|<DEF>|<SPD>|<Loot>...(Optional)
#item <Type>|<Name>|<Value>|<PARAMS>
#item Potion|<Name>|<Value>|<Magnitude>
#item BuffPotion|<Name>|<Value>|<Magnitude>|<Stat>
#item Weapon|<Name>|<Value>|<Damage>|<Ranged?>
#loot <ItemName>|<Count>
